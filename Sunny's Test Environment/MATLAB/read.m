close all;
clear all;


% filename = '../LOGGER00.CSV';
% filename = '../LOGGER01.CSV';
filename = './Logger03.CSV';
fileID = fopen(filename);

% Find length of file to specify range for individual csvread
% And also calculate timeframe 
[status, result] = system( ['wc -l < ', filename] );
numlines = str2num( result );
% x = csvread(filename,1,4,[1 4 numlines-1 4]);

resolution = 10; % data points per second, set by Sunny when taking data
sec2hour = 3600; % seconds per hour
hoursasleep = numlines/(resolution*sec2hour); % duration of data collection

sleepdata = csvread(filename,1,4); % problem with time formatting due to colons

% Need to account for shaking around prior sleeping and after waking 
% (getting device out/in the box). Could design sensing for major shaking.
% For now, set time exclusion - upper bound of 5 minutes into recording and
% 5 minutes before end of recording

exclusion = 5; % 5 minute exclusion zone
exclusionsec = exclusion * 60;
cut = exclusionsec * resolution; % this many samples to be cut off from beginning and end
sleepstart = cut;
sleepend = numlines - cut;
t = linspace(1,hoursasleep,numlines - 2 * cut + 1); % time axis

% To set without cutting
% sleepstart = 0; sleepend = numlines;
% t = linspace(1,hoursasleep,numlines - 1);
 
% x = sleepdata(sleepstart:sleepend,1);
% y = sleepdata(sleepstart:sleepend,2);
% z = sleepdata(sleepstart:sleepend,3);
net = sleepdata(sleepstart:sleepend,4);

tlabel = filename(4:11); % file name for plot generated

% Plot for individual x, y, z directions. This plot made redundant as net 
% provides the same info.

% plot(t,x);
% hold on
% plot(t,y);plot(t,z);
% xlabel('Hours of sleep');
% ylabel('Activity');
% title(tlabel);
% legend('x','y','z','Location','northeast');
% savefig(tlabel);
% print([tlabel],'-dpng'); % save as 'filename.png'

% Design interval for xticks
cycle = 1.5; % standard value taken for length of sleep cycle in hours
numcycles = ceil(hoursasleep/cycle);
intervals = linspace(0,numcycles*cycle,numcycles+1);

figure; plot(t,net);
ylim([7,13]);
xlabel('Hours of sleep');
ylabel('Activity');
legend('net activity','Location','northeast');
tlabel = [tlabel 'net'];
title(tlabel);
ax = gca;
ax.XTick = intervals;
savefig([tlabel]);
print([tlabel],'-dpng'); % save as 'filename.png'

% max_net = max(net);
% thres = 1.8*max_net;
% findpeaks(net,'MinPeakProminence',thres);
% [pks, locs] = findpeaks(Y,'MinPeakProminence',thres);


% Alarm control
% Establish baseline
% If baseline exceeded, see if time is between +/- 30 mins of alarm. 
% If not, continue. 
% If true, alarm goes off. 
% Background cond: if alarm time + 30 mins is exceeded, alarm anyway.
