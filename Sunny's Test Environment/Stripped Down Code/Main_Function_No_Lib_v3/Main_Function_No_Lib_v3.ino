/*
Summary of Functions:
String processxyz(float ax, float ay, float az) (takes float)
Takes given the input and transforms them into string
void timerLocalDelay() delays without delaying the whole program
Function must be within this function.

PinOut Directory:
Fx/Module   Takes   PinOut
SD                  10 (CS), 11 (MOSI), 12 (MISO), 13 (CLK)
Accelerometer       20 (SDA), 21 (SCL)
Piezo               8



Note: Most delays will now be local millis loop functions, to prevent
actual delay of the main function (should have caught this sooner).

If this file is named v2, then it is currently coded for: Arduino Uno!

Library Setup
*/
#include <SD.h>                  //SD Card Library
#include <SPI.h>                 //SPI Library (for MOSI, MISO and CLK)
#include <Wire.h>                //I2C Library (remember, ARM processors use Wire1.begin()!!!!)
#include "RTClib.h"              //RTC (Real Time Clock) Library
#include "Arduino.h"             //Arduino Library (not sure if this needs to be called)
#include <Adafruit_Sensor.h>     //Accelerometer Sensor comes from Adafruit, Libraries provided
#include <Adafruit_ADXL345_U.h>  //ADXL345 Sensor library from Adafruit used
/*
DATA LOGGER SETUP

Define all the necessary variables:
*/

// how many milliseconds between grabbing data and logging it. 1000 ms is once a second
#define LOG_INTERVAL  250 // mills between entries (reduce to take more/faster data) (is half a second fast enough to capture this?)

// how many milliseconds before writing the logged data permanently to disk
// set it to the LOG_INTERVAL to write each time (safest)
// set it to 10*LOG_INTERVAL to write all data every 10 datareads, you could lose up to
// the last 10 reads if power is lost but it uses less power and is much faster!
#define SYNC_INTERVAL 2500 // mills between calls to flush() - to write data to the card 
                            //(two and a half seconds between each data write routine)
uint32_t syncTime = 0; // time of last sync()

// Settings will be here

#define ECHO_TO_SERIAL       1  // Echo data to serial port
#define WAIT_TO_START        0  // Wait for serial input in setup(), Disable when using battery!

RTC_DS1307 RTCBoard; // define the Real Time Clock object

// for the data logging shield, we use digital pin 10 for the SD cs line
#define chipSelect           10  // Chip Select pin is 10 for the SD cs line

// Set the piezo pin
#define piezo                8 //constant piezo pin value

// the logging file
File logfile;


/*
Accelerometer Setup

*/

float ax;
float ay;
float az;

Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);


void error(char *str)
{
  Serial.print("error: ");
  Serial.println(str);

  while (1);
}

void displaySensorDetails(void)
{
  sensor_t sensor;
  accel.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" m/s^2");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" m/s^2");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" m/s^2");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

void displayDataRate(void)
{
  Serial.print  ("Data Rate:    ");

  switch (accel.getDataRate())
  {
    case ADXL345_DATARATE_3200_HZ:
      Serial.print  ("3200 ");
      break;
    case ADXL345_DATARATE_1600_HZ:
      Serial.print  ("1600 ");
      break;
    case ADXL345_DATARATE_800_HZ:
      Serial.print  ("800 ");
      break;
    case ADXL345_DATARATE_400_HZ:
      Serial.print  ("400 ");
      break;
    case ADXL345_DATARATE_200_HZ:
      Serial.print  ("200 ");
      break;
    case ADXL345_DATARATE_100_HZ:
      Serial.print  ("100 ");
      break;
    case ADXL345_DATARATE_50_HZ:
      Serial.print  ("50 ");
      break;
    case ADXL345_DATARATE_25_HZ:
      Serial.print  ("25 ");
      break;
    case ADXL345_DATARATE_12_5_HZ:
      Serial.print  ("12.5 ");
      break;
    case ADXL345_DATARATE_6_25HZ:
      Serial.print  ("6.25 ");
      break;
    case ADXL345_DATARATE_3_13_HZ:
      Serial.print  ("3.13 ");
      break;
    case ADXL345_DATARATE_1_56_HZ:
      Serial.print  ("1.56 ");
      break;
    case ADXL345_DATARATE_0_78_HZ:
      Serial.print  ("0.78 ");
      break;
    case ADXL345_DATARATE_0_39_HZ:
      Serial.print  ("0.39 ");
      break;
    case ADXL345_DATARATE_0_20_HZ:
      Serial.print  ("0.20 ");
      break;
    case ADXL345_DATARATE_0_10_HZ:
      Serial.print  ("0.10 ");
      break;
    default:
      Serial.print  ("???? ");
      break;
  }
  Serial.println(" Hz");
}

void displayRange(void)
{
  Serial.print  ("Range:         +/- ");

  switch (accel.getRange())
  {
    case ADXL345_RANGE_16_G:
      Serial.print  ("16 ");
      break;
    case ADXL345_RANGE_8_G:
      Serial.print  ("8 ");
      break;
    case ADXL345_RANGE_4_G:
      Serial.print  ("4 ");
      break;
    case ADXL345_RANGE_2_G:
      Serial.print  ("2 ");
      break;
    default:
      Serial.print  ("?? ");
      break;
  }
  Serial.println(" g");
}


void setup()
{
  Serial.begin(9600);
  Serial.println();
  Serial.println("------------------------------------");
  Serial.println("You are running Master G Sensei.");
  Serial.println("Accerometer Datalogger");
  Serial.println("------------------------------------");
  Serial.println(" ");

  //Wait to start. Type a character into serial to start.

#if WAIT_TO_START
  Serial.println("Type any character to start...");
  while (!Serial.available());
#endif //WAIT_TO_START


  // if piezo fails you'll hear/not hear it fail, no point in using
  // playfail

  // initialize the SD card
#if ECHO_TO_SERIAL
  Serial.print("Initializing SD card...");
#endif //ECHO_TO_SERIAL
  // make sure that the default chip select pin is set to
  // output, even if you don't use it: cs is 10 on adafruit
  pinMode(10, OUTPUT);

  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    error("Card failed, or not present");

  }
#if ECHO_TO_SERIAL
  Serial.println("card initialized.");
  //Begin accelerometer after card is initialised
  Serial.println("Accelerometer Test"); Serial.println("");
#endif //ECHO_TO_SERIAL

  /* Initialise the sensor */
  if (!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
    while (1);
  }

  /* Display some basic information on this sensor */
#if ECHO_TO_SERIAL  
  displaySensorDetails();
  
  /* Display additional settings (outside the scope of sensor_t) */
  displayDataRate();
  displayRange();
  Serial.println("");
#endif //ECHO_TO_SERIAL
  // create a new file
  char filename[] = "LOGGER00.CSV";
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i / 10 + '0';
    filename[7] = i % 10 + '0';
    if (! SD.exists(filename)) {
      // only open a new file if it doesn't exist
      logfile = SD.open(filename, FILE_WRITE);
      break;  // leave the loop!
    }
  }

  if (! logfile) {
    error("couldnt create file");
  }
#if ECHO_TO_SERIAL
  Serial.print("Logging to: ");
  Serial.println(filename);
#endif //ECHO_TO_SERIAL
  Wire.begin();

  RTCBoard.begin();

  if (! RTCBoard.isrunning()) {
    Serial.println("RTC is NOT running!");

    logfile.println("RTCBoard failed");
#if ECHO_TO_SERIAL
    Serial.println("RTCBoard failed");
#endif  //ECHO_TO_SERIAL

  }
  // following line sets the RTC to the date & time this sketch was compiled
  RTCBoard.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));


  logfile.println("millis,stamp,date,time,x,y,z,net");
#if ECHO_TO_SERIAL
  Serial.println("millis,stamp,date,time,x,y,z,net");
#endif //ECHO_TO_SERIAL

  /*
    At this stage, everything should be set up.
  */
  
#if ECHO_TO_SERIAL
  Serial.println("Setupo complete.");
#endif //ECHO_TO_SERIAL

}

void timeStampLog() {
  DateTime now = RTCBoard.now();

  // log milliseconds since starting
  uint32_t m = millis();
  logfile.print(m + 1);           // milliseconds since start actually 999, or otherwise but changed for
                                  // aesthetics
  logfile.print(", ");
#if ECHO_TO_SERIAL
  Serial.print(m + 1);         // milliseconds since start
  Serial.print(", ");
#endif //ECHO_TO_SERIAL

  // fetch the time
  now = RTCBoard.now();
  // log time
  logfile.print(now.unixtime()); // seconds since 1/1/1970
  logfile.print(", ");
  logfile.print(' ');
  logfile.print(now.year(), DEC);
  logfile.print("/");
  logfile.print(now.month(), DEC);
  logfile.print("/");
  logfile.print(now.day(), DEC);
  logfile.print(", ");
  logfile.print(now.hour(), DEC);
  logfile.print(":");
  logfile.print(now.minute(), DEC);
  logfile.print(":");
  logfile.print(now.second(), DEC);
#if ECHO_TO_SERIAL
  Serial.print(now.unixtime()); // seconds since 1/1/1970
  Serial.print(", ");
  Serial.print(' ');
  Serial.print(now.year(), DEC);
  Serial.print("/");
  Serial.print(now.month(), DEC);
  Serial.print("/");
  Serial.print(now.day(), DEC);
  Serial.print(", ");
  Serial.print(now.hour(), DEC);
  Serial.print(":");
  Serial.print(now.minute(), DEC);
  Serial.print(":");
  Serial.print(now.second(), DEC);
#endif //ECHO_TO_SERIAL
}

String floatConvert(float inputFloat){
    char bufferString[16];
    // can't be longer than the display
    //sprintf(bufferString, "%f", inputFloat);
    dtostrf(inputFloat, 7, 5, bufferString);
return String(bufferString);
}

void logaccelerometer(float ax, float ay, float az, float netxyz)
{
  logfile.print(", ");
  logfile.print(floatConvert(ax));
  logfile.print(", ");
  logfile.print(floatConvert(ay));
  logfile.print(", ");
  logfile.print(floatConvert(az));
  logfile.print(", ");
  logfile.println(floatConvert(netxyz));

#if ECHO_TO_SERIAL
  Serial.print(", ");
  Serial.print(floatConvert(ax));
  Serial.print(", ");
  Serial.print(floatConvert(ay));
  Serial.print(", ");
  Serial.print(floatConvert(az));
  Serial.print(", ");
  Serial.println(floatConvert(netxyz));
  Serial.println(" ");
#endif //ECHO_TO_SERIAL

}

float tenpointbuffer[10];


boolean alarmAlg(float netxyz){
 //make a ten point moving average 
 //make a stacking buffer to hold the ten point average
 
 //average the values to obtain moving baseline
 //If baseline above the acceptable value, detect if within timeframe of alarm and trigger output from boolean false to true.
 
 return false;
  
}


void loop() {

  // delay for the amount of time we want between readings
  delay((LOG_INTERVAL - 1) - (millis() % LOG_INTERVAL));


  timeStampLog();

  sensors_event_t event;
  accel.getEvent(&event);

  ax = event.acceleration.x - 0.48053; // takes the accelerometer x values
  ay = event.acceleration.y - 0.21575; // takes the accelerometer y values
  az = event.acceleration.z + 0.93479; // takes the accelerometer z values

  double netxyz = sqrt(sq(ax) + sq(ay) + sq(az));
  
  logaccelerometer(ax, ay, az, netxyz); // logs the data to SD card
  
  // Now we write data to disk! Don't sync too often - requires 2048 bytes of I/O to SD card
  // which uses a bunch of power and takes time
  if ((millis() - syncTime) < SYNC_INTERVAL) return;
  syncTime = millis();
  logfile.flush();

}
