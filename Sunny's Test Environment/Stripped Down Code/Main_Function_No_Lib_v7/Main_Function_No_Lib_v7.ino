/*

DO NOT MODIFY THIS FILE! COPY THIS AND THEN MODIFY IT IF YOU WANT
TO USE IT.

This code has only been tested on Arduino 1.6.4

Tested on Arduino Pro Mini (AVR)

Summary of Functions:
String processxyz(float ax, float ay, float az) (takes float)
  Takes given the input and transforms them into string
void timerLocalDelay()
  Delays without delaying the whole program
  Function must be within this function.

PinOut Directory:
Fx/Module           | PinOut
SD                    2 (CS), 11 (MOSI), 12 (MISO), 13 (CLK)
Accelerometer         A4 (SDA), A5 (SCL), 4 (CS)
Bluetooth             8 (Tx Blu, Rx Ard) 7 (Rx Blu, Tx Ard)
Piezo (opt. extra)    7

Note: Most delays will now be local millis loop functions, to prevent
actual delay of the main function (should have caught this sooner).

Library Setup
*/
//#include <Arduino.h>           //Arduino Library (doesn't need to be called)
//Standard Libraries
#include <SD.h>                  //SD Card Library
#include <SPI.h>                 //SPI Library (for SS, MOSI, MISO and CLK)
#include <Wire.h>                //I2C Library (remember, ARM processors use Wire1.begin())
#include <SoftwareSerial.h>      //Tx and Rx to Bluetooth module without using busy Serial line

//Non-standard libraries (may break down code to prevent library call for optimisation)
#include <RTClib.h>              //RTC (Real Time Clock) Library (may deprecate to save space)
#include <Adafruit_Sensor.h>     //Accelerometer Sensor comes from Adafruit, Libraries provided
#include <Adafruit_ADXL345_U.h>  //ADXL345 Sensor library from Adafruit used
/*
DATA LOGGER SETUP

Define all the necessary variables:
*/

// how many milliseconds between grabbing data and logging it. 1000 ms is once a second
#define LOG_INTERVAL  100
/* mills between entries (reduce to take more/faster data)
(is half a second fast enough to capture this?)*/

// how many milliseconds before writing the logged data permanently to disk
// set it to the LOG_INTERVAL to write each time (safest)
// set it to 10*LOG_INTERVAL to write all data every 10 datareads, you could lose up to
// the last 10 reads if power is lost but it uses less power and is much faster!
#define SYNC_INTERVAL 1000 // mills between calls to flush() - to write data to the card 
//(two and a ha/lf seconds between each data write routine)
uint32_t syncTime = 0; // time of last sync()

// Settings will be here

#define ECHO_TO_SERIAL       1  // Echo data to serial port
#define WAIT_TO_START        0  // Wait for serial input in setup(), Disable when using battery!

RTC_DS1307 RTCBoard; // define the Real Time Clock object

// SPI ChipSelect setup
#define SDchipSelect           10  // Chip Select pin is 2 for the SD cs line 

// If multiple SPI line, use a map for SPI chip select

// the logging file
File logfile;

//Bluetooth Module SoftwareSerial

SoftwareSerial BluetoothSerial(8, 7); //Rx and Tx respectively

/*
Accelerometer Variable/Instance Setup
*/
//  Variable Setup
float ax;
float ay;
float az;
//  Instance Setup
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

/*
Error function. Prints error with passed error message
*/
void error(char *str)
{
  Serial.print("error: ");
  Serial.println(str);
  while (1);
}

/*
  Serial Setup
*/
#define BAUD_RATE 9600

void serialSetup() {
  //begin Serial
  Serial.begin(BAUD_RATE);
#if ECHO_TO_SERIAL
  Serial.println("Serial OK");
#endif //ECHO_TO_SERIAL
  //begin Bluetooth Serial
  BluetoothSerial.begin(BAUD_RATE);
#if ECHO_TO_SERIAL
  BluetoothSerial.println("Bluetooth OK");
#endif //ECHO_TO_SERIAL 
}

/*
  SD Card and Log File Setup
*/
void sdcardSetup() {
  // initialize the SD card
  // make sure that the default chip select pin is set to
  // output, even if you don't use it: cs is 10 on adafruit
  digitalWrite(10, OUTPUT);
  // see if the card is present and can be initialized:
  if (!SD.begin(SDchipSelect)) {
    error("Card failed, or not present");
  }
  // create a new file
  char filename[] = "LOGGER00.CSV";
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i / 10 + '0';
    filename[7] = i % 10 + '0';
    if (! SD.exists(filename)) {
      // only open a new file if it doesn't exist
      logfile = SD.open(filename, FILE_WRITE);
      break;  // leave the loop!
    }
  }

  if (!logfile) {
    error("couldnt create file");
  }
}
/*
  Sensor Setup (Accelerometer and Light Sensors)
*/
void sensorSetup() {
  /* Initialise the accelerometer */
  if (!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println("Ooops, no ADXL345 detected ... Check your wiring!");
    while (1);
  }
  /* Initialise the analog pins for other sensors */

}
/*
  RTC Board Setup
*/
void rtcSetup() {
  RTCBoard.begin();

  if (!RTCBoard.isrunning()) {
    Serial.println("RTC is NOT running!");

    logfile.println("RTCBoard failed");
#if ECHO_TO_SERIAL
    Serial.println("RTCBoard failed");
#endif  //ECHO_TO_SERIAL
  }

  // following line sets the RTC to the date & time this sketch was compiled
  RTCBoard.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

  logfile.println("millis,date,time,x,y,z,net,temp,light");
#if ECHO_TO_SERIAL
  Serial.println("millis,date,time,x,y,z,net,temp,light");
#endif //ECHO_TO_SERIAL
}


void setup()
{
  Wire.begin();
  delay(100); //Arduino recover.
  serialSetup();
  delay(100); //Arduino recover.
  sdcardSetup();
  delay(100); //Arduino recover.
  sensorSetup();
  delay(100);
  rtcSetup();
  delay(100);
  /*
  Delay before calibration so that the person
  has time to settle down after turning the Arduino on.
  */

  //delay(1000);

  //Calibrate the accerometer
  zeroCalibration();
  delay(100);
  /*
    At this stage, everything should be set up.
  */

#if ECHO_TO_SERIAL
  Serial.println("Setup complete.");
  Serial.println("Starting in Bluetooth Communication Mode:");
  Serial.println("0 for Logging, 1 for SD card transfer, 2 for Bluetooth Communication:");
  BluetoothSerial.println("Setup complete. #");
  BluetoothSerial.println("Starting in Bluetooth Communication Mode: #");
  BluetoothSerial.println("0 for Logging, 1 for SD card transfer, 2 for Bluetooth Communication: #");

#endif //ECHO_TO_SERIAL\
}

  int ControllerMode = 2;

  //default is not logging mode

  void controllerMode() {
    while (BluetoothSerial.available()) {
      if (BluetoothSerial.read() == '0') {
        ControllerMode = 0;
      }
      else if (BluetoothSerial.read() == '1' ) {
        ControllerMode = 1;
      }
      else if (BluetoothSerial.read() == '2' ) {
        ControllerMode = 2;
      }
    }
  }

  //  String RxSerial ;
  //  String RxBluetooth ;

  void BluetoothCommunication() {
    if (BluetoothSerial.peek() == '0') {
      ControllerMode = 0;
    }
    else if ( BluetoothSerial.peek() == '1' ) {
      ControllerMode = 1;
    }
    else if (Serial.peek() == '0') {
      ControllerMode = 0;
    }
    else if (Serial.peek() == '1') {
      ControllerMode = 1;
    }
    else {
      //Note that the 'while' will keep reading until string is finished, while 'if'
      // will do a FIFO-like dump of the text every loop - Careful!

      while (BluetoothSerial.available()) {
        delay(3);
        Serial.print(BluetoothSerial.readString());
      }
      while (Serial.available()) {
        delay(3);
        BluetoothSerial.print(Serial.readString());
      }
    }
  }
  void timeStampLog() {
    DateTime now = RTCBoard.now();

    // log milliseconds since starting
    uint32_t m = millis();
    logfile.print(m);           // milliseconds since start actually 999,
    // or otherwise but changed for aesthetics
    logfile.print(", ");
#if ECHO_TO_SERIAL
    Serial.print(m);         // milliseconds since start
    Serial.print(", ");
#endif //ECHO_TO_SERIAL

    // fetch the time
    now = RTCBoard.now();
    // log time
    logfile.print(now.year(), DEC);
    logfile.print("/");
    logfile.print(now.month(), DEC);
    logfile.print("/");
    logfile.print(now.day(), DEC);
    logfile.print(", ");
    logfile.print(now.hour(), DEC);
    logfile.print(":");
    logfile.print(now.minute(), DEC);
    logfile.print(":");
    logfile.print(now.second(), DEC);
#if ECHO_TO_SERIAL
    Serial.print(now.year(), DEC);
    Serial.print("/");
    Serial.print(now.month(), DEC);
    Serial.print("/");
    Serial.print(now.day(), DEC);
    Serial.print(", ");
    Serial.print(now.hour(), DEC);
    Serial.print(":");
    Serial.print(now.minute(), DEC);
    Serial.print(":");
    Serial.print(now.second(), DEC);
#endif //ECHO_TO_SERIAL
  }

  String floatConvert(float inputFloat) {
    char bufferString[16];
    // can't be longer than the display
    //sprintf(bufferString, "%f", inputFloat);
    dtostrf(inputFloat, 7, 5, bufferString);
    return String(bufferString);
  }

  void logvalues(float ax, float ay, float az, float netxyz, float temp, float light)
  {
    logfile.print(", ");
    logfile.print(floatConvert(ax));
    logfile.print(", ");
    logfile.print(floatConvert(ay));
    logfile.print(", ");
    logfile.print(floatConvert(az));
    logfile.print(", ");
    logfile.print(floatConvert(netxyz));
    logfile.print(", ");
    logfile.print(floatConvert(temp));
    logfile.print(", ");
    logfile.println(floatConvert(light));
    
#if ECHO_TO_SERIAL
    Serial.print(", ");
    Serial.print(floatConvert(ax));
    Serial.print(", ");
    Serial.print(floatConvert(ay));
    Serial.print(", ");
    Serial.print(floatConvert(az));
    Serial.print(", ");
    Serial.print(floatConvert(netxyz));
    Serial.print(", ");
    Serial.print(floatConvert(temp));
    Serial.print(", ");
    Serial.print(floatConvert(light));
    Serial.println(" ");
#endif //ECHO_TO_SERIAL

  }

  float zerovaluex;  // x values to be modified to zero
  float zerovaluey;  // y values to be modified to zero
  float zerovaluez;  // z values to be modified to negative g

  void zeroCalibration() {
    sensors_event_t event;
    accel.getEvent(&event);
    zerovaluex = 0 - event.acceleration.x;
    zerovaluey = 0 - event.acceleration.y;
    zerovaluez = 9.8 - event.acceleration.z;
  }
  int lightLevel, high = 0, low = 1023;
  const int sensorPin0 = 0;
  const int sensorPin1 = 1;
  float voltage, degreesC;
  void sensorTempLight(){
     lightLevel = analogRead(sensorPin0);
     lightLevel = map(lightLevel, 0, 1023, 0, 255);
     lightLevel = constrain(lightLevel, 0, 255);
     voltage = getVoltage(sensorPin1);
     degreesC = (voltage - 0.5) * 100.0;
  }
  
  float getVoltage(int pin){
    return (analogRead(pin) * 0.004882814);
  }
  
  void dataLogging() {
    // delay for the amount of time we want between readings
    delay((LOG_INTERVAL - 1) - (millis() % LOG_INTERVAL));


    timeStampLog();

    sensors_event_t event;
    accel.getEvent(&event);

    ax = event.acceleration.x + zerovaluex; // takes the accelerometer x values
    ay = event.acceleration.y + zerovaluey; // takes the accelerometer y values
    az = (event.acceleration.z + zerovaluez) * (-1); // takes the accelerometer z values

    double netxyz = sqrt(sq(ax) + sq(ay) + sq(az));
    sensorTempLight();
    logvalues(ax, ay, az, netxyz, degreesC, lightLevel); // logs the data to SD card

    // Now we write data to disk! Don't sync too often - requires 2048 bytes of I/O to SD card
    // which uses a bunch of power and takes time
    if ((millis() - syncTime) < SYNC_INTERVAL) return;
    syncTime = millis();
    logfile.flush();

  }

  String StringOne;
  char charBuf [10];

  void getDump(String rxString) {
    StringOne = rxString + ".CSV";
    StringOne.toCharArray(charBuf, 14);
    BluetoothSerial.println("");
    BluetoothSerial.println(charBuf); // identify filename on phone
    File sdFile = SD.open(charBuf);
    if (sdFile) {
      while (sdFile.available()) {
        BluetoothSerial.print(sdFile.read());
      }
      sdFile.close();
      BluetoothSerial.print("#");
    }
    else {
      BluetoothSerial.println("error opening file");
    }
  }

  void loop() {
    if (ControllerMode == 0) {
      dataLogging();
      controllerMode();
    }
    else if (ControllerMode == 1) {
      BluetoothSerial.println("Input file name (no need to put CSV): ");
      while (BluetoothSerial.available()) {
        delay(3);
        getDump(BluetoothSerial.readString());
      }// end while
      controllerMode();
    }
    else if (ControllerMode == 2) {
      BluetoothCommunication();
    }
  }
